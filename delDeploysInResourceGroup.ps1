﻿$resourceGroupName = "my-resource-group"

$deployments = ( az group deployment list --resource-group $resourceGroupName ) | ConvertFrom-Json
foreach ($deployment in $deployments) {
    az group deployment delete --name $deployment.name --resource-group $resourceGroupName --no-wait
    Write-Host "Deployment $deployment.name deleted!"
}